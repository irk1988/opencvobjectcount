/*
 * Program to identify the number of objects in a frame (from a live stream video)
 * ObjCount.cpp
 *
 * Author: Philip Karunakaran, Immanuel Rajkumar
 */

#include <cv.h>
#include <highgui.h>

using namespace cv;

/*
 * Class to identify the number of objects in an image
 */
class ObjectCount {
private:
	Mat frame, HSVFrame, binaryFrame, processedFrame, opened, closed;

public:
	/*
	 * Function to count the number of objects in a video frame
	 * Identifies the various objects based on edges detected
	 * The captured frame undergoes various changes (erosion and dilation) to help in the counting process
	 * InRange function is used to obtain a binary image based on selected color range
	 * findContours identifies various contours based on connectedness property
	 */
	int countObjects() {
		VideoCapture cap(0);
		if (!cap.isOpened()) {
			std::cout << "Unable to open the camera";
			return 0;
		}
		namedWindow("Output", 1);

		for (int i = 0;; i++) {
			cap >> frame;
			cvtColor(frame, HSVFrame, CV_BGR2HSV); //converting the image to hsv frame

			/* Identifying objects based on colors - Color Used - Orange
			 * NOTE: inRange converts an image to binary based on the range.
			 * Those in the defined range will be white and the others black
			 */
			inRange(HSVFrame, Scalar(0, 180, 180), Scalar(22, 255, 255),
					binaryFrame);

			Mat element5(5, 5, CV_8U, cv::Scalar(1));
			/* Opening - Erosion and then dilation */
			cv::morphologyEx(binaryFrame, opened, cv::MORPH_OPEN, element5);
			/* Closing - Dilation and then erosion */
			cv::morphologyEx(opened, closed, cv::MORPH_CLOSE, element5);

			/* Find various contours based on connectedness property */
			vector<std::vector<Point> > contours;
			findContours(closed, contours, CV_RETR_LIST, CV_CHAIN_APPROX_NONE);
			std::cout << contours.size() << std::endl;

			vector<Rect> boundRect(contours.size());

			for (size_t i = 0; i < contours.size(); i++)
				boundRect[i] = boundingRect(Mat(contours[i]));

			/* Display the contours and boundary rectangles */
			RNG rng(12345);
			for (size_t i = 0; i < contours.size(); i++) {
				Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255),
						rng.uniform(0, 255));
				drawContours(closed, contours, i, color, 1, 8, vector<Vec4i>(),
						0, Point());
				rectangle(closed, boundRect[i].tl(), boundRect[i].br(), color,
						2, 8, 0);
			}

			imshow("Output", closed);

			if (waitKey(30) >= 0)
				break;
		}

		return 0;
	}
};

int main(void) {
	ObjectCount driver;
	driver.countObjects();
	std::cout << "Program Completed";
	return 0;
}

